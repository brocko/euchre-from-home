# Euchre From Home

A classic card game played through a terminal. 
Half game, half ascii art project. 
Untested and full of bugs. 
And it only works on one computer atm. 
 
Run euchre.py in Pycharm for the best experience. See dependencies (colorama lib).

# Playing Instructions

During bidding, you can order up by pressing 'o' or 'y', then hitting return.
Type in literally any other string to pass.

When picking a suit, you can type in:
* the letter h,d,c,s
* the corresponding index in that list starting from 1: 1,2,3,4
* or the unicode for the suit itself '♡','♢','♧','♤'

When playing a card, type the number corresponding index of your card starting from 1
* You may notice not all of your cards are available to play. 
* You have to follow suit if you have it.

	
# Rules

The game as configured runs a normal euchre game with stick the dealer. 
The game forces you to follow suit if you have it.
The game does (not yet) require you to have trump to order up. 
* Please use the honour system for now. 
* Remember, you can't order up if you just have the left bauer. 


# Configurable Rules

There are some rules in the Game class that you can set to True or False

# Euchre Rules

https://en.wikipedia.org/wiki/Euchre#The_deal


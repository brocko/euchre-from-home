#coding: utf8
import random
from colorama import Fore, Back, Style
import time

# unicode glyph for each individual card...yes, they exist.)
unicode_lookup = dict()
unicode_lookup['A♤'] = chr(0x1F0A1)  # 🂡
unicode_lookup['K♤'] = chr(0x1F0AE)  # 🂮
unicode_lookup['Q♤'] = chr(0x1F0AD)  # 🂭
unicode_lookup['J♤'] = chr(0x1F0AB)  # 🂫
unicode_lookup['10♤'] = chr(0x1F0AA) # 🂪
unicode_lookup['9♤'] = chr(0x1F0A9)  # 🂩
unicode_lookup['A♧'] = chr(0x1F0D1)  # 🃑
unicode_lookup['K♧'] = chr(0x1F0DE)  # 🃞
unicode_lookup['Q♧'] = chr(0x1F0DD)  # 🃝
unicode_lookup['J♧'] = chr(0x1F0DB)  # 🃛
unicode_lookup['10♧'] = chr(0x1F0DA) # 🃚
unicode_lookup['9♧'] = chr(0x1F0D9)  # 🃙
unicode_lookup['A♢'] = chr(0x1F0C1)  # 🃁
unicode_lookup['K♢'] = chr(0x1F0CE)  # 🃎
unicode_lookup['Q♢'] = chr(0x1F0CD)  # 🃍
unicode_lookup['J♢'] = chr(0x1F0CB)  # 🃋
unicode_lookup['10♢'] = chr(0x1F0CA) # 🃊
unicode_lookup['9♢'] = chr(0x1F0C9)  # 🃉
unicode_lookup['A♡'] = chr(0x1F0B1)  # 🂱
unicode_lookup['K♡'] = chr(0x1F0BE)  # 🂾
unicode_lookup['Q♡'] = chr(0x1F0BD)  # 🂽
unicode_lookup['J♡'] = chr(0x1F0BB)  # 🂻
unicode_lookup['10♡'] = chr(0x1F0BA) # 🂺
unicode_lookup['9♡'] = chr(0x1F0B9)  # 🂹

# ascii art avatars from asciiart.eu
# images may be modified from their original source
bart = dict()
bart[0] = ' |\/\/\/\/|'
bart[1] = ' |        |'
bart[2] = ' |        |'
bart[3] = ' |   (o)(o)'
bart[4] = ' C       _)'
bart[5] = '  |  ,____| '
bart[6] = '  |    /   '
bart[7] = ' /___ _\   '

homer = dict()
homer[0] = '   __𐑺__   '
homer[1] = ' /       \ '
homer[2] = '|         |'
homer[3] = 'M    (o)(o)'
homer[4] = 'C    .---_)'
homer[5] = ' | |._____|'
homer[6] = ' |  \____/ '
homer[7] = ' /_____ \  '

robot = dict()
robot[0] = '   \_____/    '
robot[1] = '   (_O O_)    '
robot[2] = '     (_o_)    '
robot[3] = '   __| |__   \)'
robot[4] = '  //_____\\\\  / '
robot[5] = ' / \_____/ \/ '
robot[6] = '|   /___\     '
robot[7] = '(\ /_____\    '

good_pup = dict()
good_pup[0] = '             '
good_pup[1] = '             '
good_pup[2] = '    .-"-.    '
good_pup[3] = '   /|6 6|\   '
good_pup[4] = '  {/(_0_)\}  '
good_pup[5] = '   _/ ^ \_   '
good_pup[6] = '  (/ /^\ \)- '
good_pup[7] = '   \'\'   \'\'    '

owl = dict()
owl[0] = '             '
owl[1] = '   , _ ,     '
owl[2] = '  ( o o )    '
owl[3] = ' /\'` \' `\'\   '
owl[4] = ' |\'\'\'\'\'\'\'|  '
owl[5] = ' (/ /^\ \)  '
owl[6] = ' |\\\\\'\'\'//|  '
owl[7] = '    " "     '

euchre_ascii = dict()
euchre_ascii[0] = '                                  88            '
euchre_ascii[1] = '                                  88            '
euchre_ascii[2] = '                                  88            '
euchre_ascii[3] = ' ,adPPYba, 88       88  ,adPPYba, 88,dPPYba,  8b,dPPYba,  ,adPPYba,  '
euchre_ascii[4] = 'a8P_____88 88       88 a8"     "" 88P\'    "8a 88P\'   "Y8 a8P_____88  '
euchre_ascii[5] = '8PP""""""" 88       88 8b         88       88 88         8PP"""""""   '
euchre_ascii[6] = '"8b,   ,aa "8a,   ,a88 "8a,   ,aa 88       88 88         "8b,   ,aa   '
euchre_ascii[7] = ' `"Ybbd8"\'  `"YbbdP\'Y8  `"Ybbd8"\' 88       88 88          `"Ybbd8" '


hearts_uni = chr(0x2661)
diamonds_uni = chr(0x2662)
clubs_uni = chr(0x2667)
spades_uni = chr(0x2664)

suits = [hearts_uni, diamonds_uni, clubs_uni, spades_uni]
suit_nums = [1,2,3,4]
suit_name = {1:"hearts",2:"diamonds",3:"clubs",4:"spades"}
suit_chars = ['♡','♢','♧','♤']
face_value = ['9', '10', 'J', 'Q', 'K', 'A']
face_numbers = [9,10,11,12,13,14]

class Player:
    def __init__(self,id,name,avatar):
        self._id = id
        self._name = name
        self._avatar = avatar

    @property
    def team(self):
        return (self._id % 2)

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

class Round:
    def __init__(self,dealer_id):
        self._dealer_id = dealer_id
        self._valid_players = {0, 1, 2, 3}
        self._valid_cards = dict()
        for x in range(0,4):
            self._valid_cards[x] = {0,1,2,3,4}
        self._bidding_team = -1
        self._trump = 'none'
        self._lone_hand = False
        self._team_tricks = dict()
        self._team_tricks[0] = 0
        self._team_tricks[1] = 0
        self._round_points = dict()
        self._round_points[0] = 0
        self._round_points[1] = 0

    @property
    def dealer_id(self):
        return self._dealer_id

    @property
    def bidding_team(self):
        return self._bidding_team

    @property
    def valid_players(self):
        return self._valid_players

    @property
    def team_tricks(self):
        return self._team_tricks

    @bidding_team.setter
    def set_bidding_team(self, team_id):
        self._bidding_team = team_id

    def invalidate_player(self, player_id):
        self._valid_players.remove(player_id)
        self._lone_hand = True

    def determine_winner_of_round(self):
        return self._round_points

    def update_bidding_info(self,bidding_info):
        self._bidding_team = bidding_info['team']
        self._trump = bidding_info['trump']

    def score_trick(self, winning_team):
        self._team_tricks[winning_team] += 1

class Game:
    def __init__(self, player_name, team_name, dealer):
        self._player_name = player_name
        self._team_name = team_name
        self._dealer = dealer
        self._round_num = 0
        self._team_round_score = dict()
        self._team_round_score[0] = dict()
        self._team_round_score[1] = dict()
        self._team_score = dict()
        self._team_score[0] = 0
        self._team_score[1] = 0
        self._next_turn = 0
        self._player_hand = dict()
        self._stick_the_dealer = True
        self._force_lone_hand_on_ordering_up_partner = True

    @property
    def valid_players(self):
        return self._valid_players

    @property
    def valid_cards(self):
        return self._valid_cards

    @property
    def player_hand(self):
        return self._player_hand[player_id]

    @property
    def player_hands(self):
        return self._player_hand

    @property
    def team_name(self,id):
        return self._team_name[id]

    @property
    def team_names(self):
        return self._team_name

    @property
    def player_name(self):
        return self._player_name

    @property
    def dealer(self):
        return self._dealer

    @property
    def dealer_name(self):
        return self._player_name[self._dealer]

    @property
    def dealer_team_name(self):
        return self._team_name[(self._dealer % 2)]

    @property
    def team_score(self):
        return self._team_score

    @property
    def stick_the_dealer(self):
        return self._stick_the_dealer

    @property
    def force_lone_hand_on_ordering_up_partner(self):
        return self._force_lone_hand_on_ordering_up_partner

    @player_hand.setter
    def player_hand(self, hand):
        self._player_hand = hand

    def check_winner(self):
        retval = 0 #no winner yet
        if self._team_score[0] >= 10:
            print("Team 0 wins!")
            print("Congratulations " + self._team_name[0])
            retval = 1
        elif self._team_score[1] >= 10:
            print("Team 1 wins!")
            print("Congratulations " + self._team_name[1])
            retval = 2
        return retval

    def increment_dealer(self):
        self._dealer = ((self._dealer + 1) % 4)

    def post_score(self,points):
        self._team_round_score[0][self._round_num] = points[0]
        self._team_round_score[1][self._round_num] = points[1]
        self._team_score[0] += points[0]
        self._team_score[1] += points[1]
        self._round_num += 1
        self.increment_dealer()

class Hand:
    def __init__(self,cards,trump):
        self._cards = cards # list of cards
        self._num_of_cards = len(cards)
        self._trump = trump

    @property
    def cards(self):
        return self._cards

    @property
    def num_of_cards(self):
        return self._num_of_cards

    @cards.setter
    def cards(self,new_cards):
        self._cards = new_cards
        self._num_of_cards = len(new_cards)

    def print_hand_horizontal(self):
        print(Fore.CYAN + "My Hand:")
        visual_string = ""
        card_color = []
        visual_line = {}
        for x in range(0,8):
            visual_line[x] = ""
        for card in self._cards:
            visual_line[0] += f'  ╔ೱೱೱೱMMೱೱೱೱ╗  '
            visual_line[1] += f'  ║ {card.face_value:<2}{card.suit}     ║  '
            visual_line[2] += f'  ║ {card.suit}       ║  '
            visual_line[3] += f'  ║   {card.face_value:<2}{card.suit}   ║  '
            visual_line[4] += f'  ║       {card.suit} ║  '
            visual_line[5] += f'  ║     {card.suit}{card.face_value:>2} ║  '
            visual_line[6] += f'  ╚ೱೱೱೱWWೱೱೱೱ╝  '
            visual_line[7] += f'{card.unicode} {card.face_value:<3}{suit_name[card.suit_number]:<8}{card.unicode} '
            card_color.append(card.color)
        card_color.reverse()
        card1_color = card_color.pop()
        if self._num_of_cards > 1:
            card2_color = card_color.pop()
        if self._num_of_cards > 2:
            card3_color = card_color.pop()
        if self._num_of_cards > 3:
            card4_color = card_color.pop()
        if self._num_of_cards > 4:
            card5_color = card_color.pop()
        if self._num_of_cards > 5:
            card6_color = card_color.pop()
        for x in range(0,8):
            if self._num_of_cards == 6:
                if x in [0,6]:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '   ' + visual_line[x][:16] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][16:32] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][32:48] + (
                              Fore.GREEN if card4_color == 'green' else Fore.RED if card4_color == 'red' else Fore.BLACK) +
                          visual_line[x][48:64] + (
                              Fore.GREEN if card5_color == 'green' else Fore.RED if card5_color == 'red' else Fore.BLACK) +
                          visual_line[x][64:80] + (
                              Fore.GREEN if card6_color == 'green' else Fore.RED if card6_color == 'red' else Fore.BLACK) +
                          visual_line[x][80:96])
                else:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '   ' + visual_line[x][:15] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][15:30] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][30:45] + (
                              Fore.GREEN if card4_color == 'green' else Fore.RED if card4_color == 'red' else Fore.BLACK) +
                          visual_line[x][45:60] + (
                              Fore.GREEN if card5_color == 'green' else Fore.RED if card5_color == 'red' else Fore.BLACK) +
                          visual_line[x][60:75]  + (
                              Fore.GREEN if card6_color == 'green' else Fore.RED if card6_color == 'red' else Fore.BLACK) +
                          visual_line[x][75:90])
            elif self._num_of_cards == 5:
                if x in [0,6]:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '        ' + visual_line[x][:16] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][16:32] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][32:48] + (
                              Fore.GREEN if card4_color == 'green' else Fore.RED if card4_color == 'red' else Fore.BLACK) +
                          visual_line[x][48:64] + (
                              Fore.GREEN if card5_color == 'green' else Fore.RED if card5_color == 'red' else Fore.BLACK) +
                          visual_line[x][64:80])
                else:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '        ' + visual_line[x][:15] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][15:30] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][30:45] + (
                              Fore.GREEN if card4_color == 'green' else Fore.RED if card4_color == 'red' else Fore.BLACK) +
                          visual_line[x][45:60] + (
                              Fore.GREEN if card5_color == 'green' else Fore.RED if card5_color == 'red' else Fore.BLACK) +
                          visual_line[x][60:75])
            elif self._num_of_cards == 4:
                if x in [0,6]:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '                 ' + visual_line[x][:16] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][16:32] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][32:48] + (
                              Fore.GREEN if card4_color == 'green' else Fore.RED if card4_color == 'red' else Fore.BLACK) +
                          visual_line[x][48:64])
                else:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '                 ' + visual_line[x][:15] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][15:30] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][30:45] + (
                              Fore.GREEN if card4_color == 'green' else Fore.RED if card4_color == 'red' else Fore.BLACK) +
                          visual_line[x][45:60])
            elif self._num_of_cards == 3:
                if x in [0,6]:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '                    ' + visual_line[x][:16] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][16:32] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][32:48])
                else:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '                    ' + visual_line[x][:15] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][15:30] + (
                              Fore.GREEN if card3_color == 'green' else Fore.RED if card3_color == 'red' else Fore.BLACK) +
                          visual_line[x][30:45])
            elif self._num_of_cards == 2:
                if x in [0,6]:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '                    ' + visual_line[x][:16] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][16:32])
                else:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                          '                    ' + visual_line[x][:15] + (
                              Fore.GREEN if card2_color == 'green' else Fore.RED if card2_color == 'red' else Fore.BLACK) +
                          visual_line[x][15:30])
            elif self._num_of_cards == 1:
                if x in [0,6]:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                        '                    ' +visual_line[x][:16])
                else:
                    print((Fore.GREEN if card1_color == 'green' else Fore.RED if card1_color == 'red' else Fore.BLACK) +
                        '                    ' + visual_line[x][:15])

class Card:
    def __init__(self,value,suit,trump):
        self._value = value
        if (value == 10):
            self._face_value = '10'
        else:
            self._face_value = '90JQKA'[value-9]
        self._suit = '♡♢♧♤'[suit-1]                    # 1,2,3,4 = ♡♢♧♤
        self._suit_number = suit
        self._value_suit = self.face_value + self._suit # primary key
        self._trump = trump                             # True, False
        self._unicode = unicode_lookup[self._value_suit]
        if trump:
            self._color = 'green'
        elif suit < 3:
            self._color = 'red'
        else:
            self._color = 'black'
        if suit == 1:
            self._suit_name = 'hearts'
        elif suit == 2:
            self._suit_name = 'diamonds'
        elif suit == 3:
            self._suit_name = 'clubs'
        elif suit == 4:
            self._suit_name = 'spades'
        else:
            self._suit_name = 'none'
        self._card_face = list()
        self._card_face.append(' ╔ೱೱೱೱMMೱೱೱೱ╗ ')
        self._card_face.append(f' ║ {self.face_value:<2}{self._suit}     ║ ')
        self._card_face.append(f' ║ {self._suit}       ║ ')
        self._card_face.append(f' ║   {self.face_value:<2}{self._suit}   ║ ')
        self._card_face.append(f' ║       {self._suit} ║ ')
        self._card_face.append(f' ║    {self._suit}{self.face_value:>2}  ║ ')
        self._card_face.append(' ╚ೱೱೱೱWWೱೱೱೱ╝ ')
        self._card_face_reverse = list(self._card_face)
        self._card_face_reverse.reverse()

    @property
    def card_face_reverse(self):
        return self._card_face_reverse

    @property
    def unicode(self):
        return self._unicode

    @property
    def face_value(self):
        return self._face_value

    @property
    def suit(self):
        return self._suit

    @property
    def suit_name(self):
        return self._suit_name

    @property
    def suit_number(self):
        return self._suit_number

    @property
    def value(self):
        return self._value

    @property
    def value_suit(self):
        return self._value_suit

    @property
    def trump(self):
        return self._trump

    @trump.setter
    def trump(self, value):
        self._trump = value
        if value:
            self._color = 'green'

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self,value):
        self._color = value

    def print(self):
        if self._trump: #green
            for x in range(len(self._card_face)):
                print(Fore.GREEN + self._card_face[x])
        elif self._suit in {'♡','♢'}: #red
            for x in range(len(self._card_face)):
                print(Fore.RED + self._card_face[x])
        else:
            for x in range(len(self._card_face)):
                print(Fore.BLACK + self._card_face[x])

    @property
    def card_face(self):
        return self._card_face

def new_deck():
    deck = []
    for face in range (9,15):
        for suit in suit_nums:
            deck.append(Card(face, suit, False))
    return deck

def print_deck(deck):
    for x in range(len(deck)):
        print(deck[x].value_suit)

def shuffle_deck(deck):
    random.seed()
    random.shuffle(deck)
    return deck

def deal_deck():
    print(Fore.BLACK + "Dealing new deck.")
    for x in range(0, 20):
        player_id = (x % 4)
        if (x % 4) == 0:
            player_hand[0].append(deck0.pop())
        if (x % 4) == 1:
            player_hand[1].append(deck0.pop())
        if (x % 4) == 2:
            player_hand[2].append(deck0.pop())
        if (x % 4) == 3:
            player_hand[3].append(deck0.pop())
    for x in range(0, 4):
        discard_pile.append(deck0.pop())

def reveal_top_card():
    #print(Fore.BLUE + "REVEALING TOP CARD:" + discard_pile[0].value_suit)
    return discard_pile[0]

def set_trump(suit):
    for p in range(0,4):
        for c in range(len(player_hand[p])):
            if suit == player_hand[p][c].suit:
                player_hand[p][c].trump = True
            elif is_left_bauer(suit,player_hand[p][c]):
                player_hand[p][c].trump = True
    return suit

def print_deck_vertical_text(deck):
    for x in range(len(deck)):
        deck[x].print()

def clear_screen_short():
    for _ in range(0,6):
        print()

def clear_screen_full():
    for _ in range(0,25):
        print()

def print_euchre():
    for x in range(0,12):
        foo = clear_screen_full()
        if (x % 6 == 0):
            for x in range(0,8):
                print(Fore.BLACK + euchre_ascii[x])
        if (x % 6 == 1):
            for x in range(0, 8):
                print(Fore.RED + euchre_ascii[x])
        if (x % 6 == 2):
            for x in range(0,8):
                print(Fore.BLUE + euchre_ascii[x])
        if (x % 6 == 3):
            for x in range(0, 8):
                print(Fore.LIGHTGREEN_EX + euchre_ascii[x])
        if (x % 6 == 4):
            for x in range(0,8):
                print(Fore.LIGHTMAGENTA_EX + euchre_ascii[x])
        if (x % 6 == 5):
            for x in range(0, 8):
                print(Fore.LIGHTYELLOW_EX + euchre_ascii[x])
        foo = clear_screen_short()
        foo = clear_screen_short()
        time.sleep(0.5)

def is_right_bauer(trump, card):
    retval = False
    if card.face_value == 'J':
        if card.suit == trump:
            retval = True
    return retval

def is_left_bauer(trump, card):
    retval = False
    if (card.suit != trump):
        if card.face_value == 'J':
            if (card.suit == '♡') & (trump == '♢'):
                retval = True
            elif (card.suit == '♢') & (trump == '♡'):
                retval = True
            elif (card.suit == '♧') & (trump == '♤'):
                retval = True
            elif (card.suit == '♤') & (trump == '♧'):
                retval = True
    return retval

def is_this_trump(trump, card):
    retval = False
    if is_left_bauer(trump,card):
        retval = True
    else:
        if card.suit == trump:
            retval = True
    return retval

def who_is_my_partner(player_id):
    return (player_id + 2) % 4

def which_team_am_i_on(player_id):
    return (player_id % 2)

def bidding(player_hand):
    trump = 'none'
    lone_hand = False
    player_to_skip = -1
    top_card = reveal_top_card()
    for p in range(0, 4):
        order_up = False
        player = (p + 1 + game.dealer) % 4  # start 1 past the dealer, then modulo 4
        player_cards[game.dealer] = Hand(player_hand[game.dealer], trump)
        print_gametable_header(player, True, '','','','',False)
        print_gametable_middle(player, True,'','')
        foo = player_cards[player].print_hand_horizontal()
        if game.force_lone_hand_on_ordering_up_partner & ((game.dealer + 2) % 4 == player):
            player_order = input(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX + " Order up? " + Fore.LIGHTRED_EX + "Your partner is dealer, so you'd have to go alone. " + Fore.LIGHTYELLOW_EX + "'Y' or 'O' to Order up. ")
            if (player_order.lower() == 'y') or (player_order.lower() == 'o'):
                order_up = True
                lone_hand = True
        else:
            player_order = input(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX + " Order up? " + Fore.YELLOW + "'Y' or 'O' to Order up. 'A' to go alone. ")
            if (player_order.lower() == 'y') or (player_order.lower() == 'o'):
                order_up = True
            elif (player_order.lower() == 'a'):
                order_up = True
                lone_hand = True
        if order_up:
            skip_dealer = False
            bidding_team = player % 2
            if lone_hand:
                #make partner sit out this round
                player_to_skip = (player + 2) % 4
                lone_hand = True
                if player_to_skip == game.dealer:
                    skip_dealer = True
            if (not skip_dealer):
                # dealer picks up top card and discards one of their 6 cards.
                # we skip this sequence if the dealer is sitting out this round.
                player_hand[game.dealer].append(top_card)
                trump = set_trump(top_card.suit)
                player_cards[game.dealer] = Hand(player_hand[game.dealer],trump)
                foo = player_cards[game.dealer].print_hand_horizontal()
                card_to_discard = input(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX + Fore.MAGENTA + " Dealer: Select card to discard: " + Fore.LIGHTYELLOW_EX + "1/2/3/4/5/6 ")
                del player_hand[game.dealer][int(card_to_discard) - 1]
                player_cards[game.dealer] = Hand(player_hand[game.dealer],trump)
                print_gametable_header(player, True, '', '', '', '', bidding_team)
                print_gametable_middle(player, True, '', '')
                print(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX + " Dealer: your new hand after discard")
                foo = player_cards[game.dealer].print_hand_horizontal()
            else:
                trump = set_trump(top_card.suit)
            break
    if trump == 'none':
        bid_trump = False
        for p in range(0, 4):
            player = (p + 1 + game.dealer) % 4  # start 1 past the dealer, then modulo 4
            print_gametable_header(player, True, '','','','',0)
            print_gametable_middle(player, True, '', '')
            foo = player_cards[player].print_hand_horizontal()
            if (p == 3) & (game.stick_the_dealer):
                print(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX + Fore.LIGHTRED_EX + "Dealer: You must pick trump. We are playing 'stick the dealer'.")
                bid_trump = True
            else:
                player_order = input(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX +  ": Set trump? " + Fore.LIGHTRED_EX + "[You can't choose " + top_card.suit + "] " + Fore.LIGHTYELLOW_EX + "Y/N ")
                if (player_order.lower() == 'y'):
                    bid_trump = True
            if bid_trump:
                player_alone = input(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX +  " Go alone? " + Fore.LIGHTYELLOW_EX + "Y/N ")
                if player_alone.lower() == 'y':
                    # make partner sit out this round
                    player_to_skip = (player + 2) % 4
                    lone_hand = True
                bidding_team = player % 2
                while (1):
                    player_trump = input(Fore.BLUE + game.player_name[player] + Fore.LIGHTMAGENTA_EX + " Set which suit to trump? " + Fore.LIGHTYELLOW_EX + "1/2/3/4 ♡/♢/♧/♤ H/D/C/S ")
                    if player_trump.upper() in ['H', '1', '♡']:
                        if (top_card.suit == '♡'):
                            print(
                                Fore.YELLOW + "You can't pick that one. " + Fore.RED + "Hearts (♡)" + Fore.YELLOW + " was turned down. Try again.")
                            continue
                        else:
                            trump = set_trump('♡')
                            break
                    elif player_trump.upper() in ['D', '2', '♢']:
                        if (top_card.suit == '♢'):
                            print(
                                Fore.YELLOW + "You can't pick that one." + Fore.RED + "Diamonds (♢)" + Fore.YELLOW + " was turned down. Try again.")
                            continue
                        else:
                            trump = set_trump('♢')
                            break
                    elif (player_trump.upper() == 'C'):
                        if (top_card.suit == '♧'):
                            print(
                                Fore.YELLOW + "You can't pick that one. " + Fore.BLACK + "Clubs (♧)" + Fore.YELLOW + " was turned down. Try again.")
                            continue
                        else:
                            trump = set_trump('♧')
                            break
                    elif (player_trump.upper() == 'S'):
                        if (top_card.suit == '♤'):
                            print(
                                Fore.YELLOW + "You can't pick that one. " + Fore.BLACK + "Spades (♤)" + Fore.YELLOW + " was turned down. Try again.")
                            continue
                        else:
                            trump = set_trump('♤')
                            break
                    else:
                        print("please try again.")
                break
    bid_info = dict()
    bid_info['team'] = bidding_team
    bid_info['trump'] = trump
    bid_info['post_bid_cards'] = player_hand
    bid_info['lone_hand'] = lone_hand
    bid_info['player_to_skip'] = player_to_skip
    game.player_hand = player_hand
    print(Fore.YELLOW + "player to skip: " + str(player_to_skip))
    return bid_info

def determine_trick_winner(trump, lead_suit, cards_played, valid_players):
    # the suit that was led is nerfed by 8 points:
    # 9 of suit that was led is worth 1 point
    # 10 of same suit is worth 2 points, etc
    # ... Ace of that suit is worth 6 points
    # trump are worth their face value
    # 9=9, 10=10, Q=12, K=13, A=14
    # right bauer (Jack of trump) is worth 16
    # left bauer (Jack of same colour) is worth 15
    # anything else is worth 0
    winner = 0
    highest = 0
    value = 0
    for p in valid_players:
        if (trump == cards_played[p].suit):
            if is_right_bauer(trump,cards_played[p]):
                value = 16
            else:
                value = cards_played[p].value
        elif is_left_bauer(trump,cards_played[p]):
            value = 15
        elif (lead_suit == cards_played[p].suit):
            value = cards_played[p].value - 8
        else:
            value = 0
        if value > highest:
            winner = p
            highest = value
    return winner

def play_card(player_id, bidding_info, lead_player, lead_suit):
    this_trump = bidding_info["trump"]
    this_bidding_team = bidding_info["team"]
    foo = player_cards[player_id].print_hand_horizontal()
    my_hand = list(player_cards[player_id].cards)
    i_have_the_lead_suit = False
    valid_cards = list()
    all_cards = list()
    display_choices_string = "["
    all_card_choices_string = "["
    my_hand.reverse()
    for x in range(0,player_cards[player_id].num_of_cards):
        this_card = my_hand.pop()
        this_suit = this_card.suit
        all_cards.append(x)
        all_card_choices_string += str(x+1) + ","
        if (this_suit == lead_suit and not is_left_bauer(this_trump,this_card)) or (lead_suit == this_trump and is_left_bauer(this_trump,this_card)):
            i_have_the_lead_suit = True
            valid_cards.append(x)
            display_choices_string = display_choices_string + str(x+1) + ","
    if (not i_have_the_lead_suit):
        valid_cards = all_cards
        display_choices_string = all_card_choices_string
    display_choices_string = display_choices_string[:-1] + "]"
    card_selection = -2
    while (card_selection - 1) not in valid_cards:
        try:
            card_selection_string = input(Fore.LIGHTBLUE_EX + player_name[player_id] + Fore.MAGENTA + " Play card " + display_choices_string + " ")
            card_selection = int(card_selection_string)
        except:
            print("invalid input. Please try again.")
            time.sleep(0.1)
            pass
    card_id = int(card_selection) - 1
    hands = game.player_hands
    popped_card = hands[player_id][card_id]
    print("Card played " + popped_card.value_suit)
    del player_hand[player_id][card_id]
    #foo = player_hand[player_id].print_hand_horizontal()
    return popped_card

def play_trick(bidding_info, player_offset, first_hand):
    this_trump = bidding_info["trump"]
    this_bidding_team = bidding_info["team"]
    lead_suit = 'n'
    player_played_card = dict()
    for x in range(0,4):
        player_played_card[x] = False
    card_played = dict()
    valid_players = {0,1,2,3}
    if bidding_info['lone_hand']:
        player_to_skip = bidding_info['player_to_skip']
        print(Fore.RED + "player to SKIP: " + str(player_to_skip))
        valid_players.remove(player_to_skip)
        print("valid players: ")
        print(valid_players)
    for p in range(0,4):
        if (first_hand):
            player = (p + 1 + game.dealer) % 4  # start 1 past the dealer, then modulo 4
        else:
            player = (p + player_offset) % 4
        if player in valid_players:
            print_gametable_header(player, False, card_played, player_played_card, lead_suit, this_trump, this_bidding_team)
            print_gametable_middle(player, False, card_played, player_played_card)
            if lead_suit == 'n':
                print("You are to lead this hand.")
                card_played[player] = play_card(player, bidding_info, True, 'none')
                if is_left_bauer(this_trump, card_played[player]):
                    lead_suit = this_trump
                else:
                    lead_suit = card_played[player].suit
            else:
                card_played[player] = play_card(player, bidding_info, False, lead_suit)
            if (len(player_hand[player])>0):
                player_cards[player] = Hand(player_hand[player], trump)
                foo = player_cards[player].print_hand_horizontal()
            player_played_card[player] = True
    print("Determining winner of trick...")
    winner = determine_trick_winner(this_trump, lead_suit, card_played, valid_players)
    print(Fore.MAGENTA + "winner is " + Fore.BLUE + player_name[winner] + Fore.MAGENTA + " with " + Fore.BLACK + card_played[winner].value_suit)
    winning_team = winner % 2
    round.score_trick(winning_team)
    return winner

def play_round(bidding_info):
    print(Fore.BLACK + "Starting round")
    player_offset = 1
    tricks_team = dict()
    tricks_team[0] = 0
    tricks_team[1] = 0
    for h in range(0,5):
        winner_id = play_trick(bidding_info, player_offset, h == 0)
        trick_winning_team = winner_id % 2
        tricks_team[trick_winning_team] += 1
        print(Fore.GREEN + "Round Score Update")
        print(team_name[0] + ": " + str(tricks_team[0]) + " tricks.")
        print(team_name[1] + ": " + str(tricks_team[1]) + " tricks.")
        player_offset = winner_id
    bidding_team = bidding_info['team']
    other_team = (bidding_team + 1) % 2
    points_team = dict()
    if tricks_team[bidding_team] < 3:
        points_team[bidding_team] = 0
        points_team[other_team] = 2
        result_string = "bidding team was EUCHRED!"
        print_euchre()
    elif tricks_team[bidding_team] == 5:
        if (bidding_info['lone_hand']):
            points_team[bidding_team] = 4
            points_team[other_team] = 0
            result_string = "Lone had met their bid and got all tricks."
        else:
            points_team[bidding_team] = 2
            points_team[other_team] = 0
            result_string = "Bidding team got all the tricks."
    else:
        points_team[bidding_team] = 1
        points_team[other_team] = 0
        result_string = "Bidding team met their bid."
    print(Fore.YELLOW + "Round Results")
    print("Bidding Team: " + Fore.BLUE + team_name[bidding_team])
    print(result_string)
    print(Fore.LIGHTYELLOW_EX + "Team0 - " + Fore.BLUE + team_name[0] + Fore.YELLOW + " points awarded: " + Fore.BLACK + str(points_team[0]))
    print(Fore.LIGHTYELLOW_EX + "Team1 - " + Fore.BLUE + team_name[1] + Fore.YELLOW + " points awarded: " + Fore.BLACK + str(points_team[1]))
    game.post_score(points_team)
    return points_team

def print_gametable_header(player, bidding, card_played, player_played_card, lead_suit, trump, bidding_team):
    foo = clear_screen_short()
    pre_header = '                     ---===𑁔𑁔𑁔  EUCHRE  FROM  HOME  𑁔𑁔𑁔===---                   '
    print(Fore.RED + pre_header)
    my_partner = who_is_my_partner(player)
    my_team = which_team_am_i_on(player)
    their_team = (my_team + 1) % 2
    game.dealer_name
    if (bidding_team == my_team):
        b0 = 'B'
        b1 = ' '
    else:
        b0 = ' '
        b1 = 'B'
    visual_line = dict()
    visual_line[0] = dict()
    visual_line[0][0] = f'+==================+'
    visual_line[0][1] = f'║    GAME SCORE    ║'
    visual_line[0][2] = f'║     You:  {game.team_score[my_team]:<2}     ║'
    visual_line[0][3] = f'║    Them:  {game.team_score[their_team]:<2}     ║'
    visual_line[0][4] = f'+==================+'
    visual_line[0][5] = f'║    Dealer:       ║'
    visual_line[0][6] = f'║ {game.dealer_name:<17}║'
    visual_line[0][7] = f'║ {game.dealer_team_name:<17}║'
    visual_line[0][8] = f'+==================+'
    visual_line[1] = dict()
    visual_line[1][0] = f'{player_avatar[my_partner][0]:<15}'
    visual_line[1][1] = f'{player_avatar[my_partner][1]:<15}'
    visual_line[1][2] = f'{player_avatar[my_partner][2]:<15}'
    visual_line[1][3] = f'{player_avatar[my_partner][3]:<15}'
    visual_line[1][4] = f'{player_avatar[my_partner][4]:<15}'
    visual_line[1][5] = f'{player_avatar[my_partner][5]:<15}'
    visual_line[1][6] = f'{player_avatar[my_partner][6]:<15}'
    visual_line[1][7] = f'{player_avatar[my_partner][7]:<15}'
    visual_line[1][8] = f'{player_name[my_partner]:<15}'
    if (not bidding):
        if (player_played_card[my_partner]):
            partner_card = card_played[my_partner]
            visual_line[2] = dict()
            visual_line[2][0] =  '               '
            visual_line[2][1] = f'  ╔ೱೱೱೱMMೱೱೱೱ╗  '
            visual_line[2][2] = f'  ║ {partner_card.face_value:<2}{partner_card.suit}     ║  '
            visual_line[2][3] = f'  ║ {partner_card.suit}       ║  '
            visual_line[2][4] = f'  ║   {partner_card.face_value:<2}{partner_card.suit}   ║  '
            visual_line[2][5] = f'  ║       {partner_card.suit} ║  '
            visual_line[2][6] = f'  ║     {partner_card.suit}{partner_card.face_value:>2} ║  '
            visual_line[2][7] = f'  ╚ೱೱೱೱWWೱೱೱೱ╝  '
            visual_line[2][8] = f'{partner_card.unicode} {partner_card.face_value:<3}{suit_name[partner_card.suit_number]:<8}{partner_card.unicode} '
        visual_line[3] = dict()
        visual_line[3][0] = f'+=============+  '
        visual_line[3][1] = f'║ ROUND SCORE ║  '
        visual_line[3][2] = f'║  You:  {round.team_tricks[my_team]:<2} {b0} ║  '
        visual_line[3][3] = f'║ Them:  {round.team_tricks[their_team]:<2} {b1} ║  '
        visual_line[3][4] = f'+=============+  '
        visual_line[3][5] = f'║ Trump: {trump}    ║  '
        visual_line[3][6] = f'║ Lead:  {lead_suit}    ║  '
        visual_line[3][7] = f'+=============+  '
        visual_line[3][8] = f'                '
        for x in visual_line[0]:
            if (player_played_card[my_partner]):
                print(Fore.LIGHTGREEN_EX + ' ' + visual_line[0][x] + '           ' +
                      Fore.BLUE + visual_line[1][x] +
                      Fore.BLACK + visual_line[2][x] + '                   ' +
                      Fore.MAGENTA + visual_line[3][x])
            else:
                print(Fore.LIGHTGREEN_EX + ' ' + visual_line[0][x] + '           ' +
                      Fore.BLUE + visual_line[1][x] + '                            ' +
                      Fore.MAGENTA + visual_line[3][x])
    else:
        for x in range(0,9):
            print(Fore.LIGHTGREEN_EX + ' ' + visual_line[0][x] + '           ' +
                  Fore.BLUE + visual_line[1][x])
    for x in range(0,2):
        print()

def print_gametable_middle(player, bidding, card_played, player_played_card):
    to_my_left = (player + 1) % 4
    to_my_right = (player + 3) % 4
    visual_line = dict()
    visual_line[0] = dict()
    visual_line[0][0] = f'{player_avatar[to_my_left][0]:<15}'
    visual_line[0][1] = f'{player_avatar[to_my_left][1]:<15}'
    visual_line[0][2] = f'{player_avatar[to_my_left][2]:<15}'
    visual_line[0][3] = f'{player_avatar[to_my_left][3]:<15}'
    visual_line[0][4] = f'{player_avatar[to_my_left][4]:<15}'
    visual_line[0][5] = f'{player_avatar[to_my_left][5]:<15}'
    visual_line[0][6] = f'{player_avatar[to_my_left][6]:<15}'
    visual_line[0][7] = f'{player_avatar[to_my_left][7]:<15}'
    visual_line[0][8] = f'{player_name[to_my_left]:<15}'
    if bidding:
        visual_line[3] = dict()
        visual_line[3][0] = "Top card for bidding"
        top_card = reveal_top_card()
        card_face = list(top_card.card_face_reverse)
        for x in range(1,8):
            if x in (1,7):
                #the top and bottom rows have an extra unicode char
                visual_line[3][x] = f'{card_face.pop():<21}'
            else:
                visual_line[3][x] = f'{card_face.pop():<20}'
        visual_line[3][8] = f' {top_card.unicode} {top_card.face_value:>2} {top_card.suit_name:<8}      '

    else:
        visual_line[2] = dict()
        if (player_played_card[to_my_left]):
            partner_card = card_played[to_my_left]
            visual_line[2][0] =  '               '
            visual_line[2][1] = f'  ╔ೱೱೱೱMMೱೱೱೱ╗  '
            visual_line[2][2] = f'  ║ {partner_card.face_value:<2}{partner_card.suit}     ║  '
            visual_line[2][3] = f'  ║ {partner_card.suit}       ║  '
            visual_line[2][4] = f'  ║   {partner_card.face_value:<2}{partner_card.suit}   ║  '
            visual_line[2][5] = f'  ║       {partner_card.suit} ║  '
            visual_line[2][6] = f'  ║     {partner_card.suit}{partner_card.face_value:>2} ║  '
            visual_line[2][7] = f'  ╚ೱೱೱೱWWೱೱೱೱ╝  '
            visual_line[2][8] = f'{partner_card.unicode} {partner_card.face_value:<3}{suit_name[partner_card.suit_number]:<8}{partner_card.unicode} '
        else:
            for x in range(0,9):
                visual_line[2][x] = '               '
        visual_line[5] = dict()
        if (player_played_card[to_my_right]):
            partner_card = card_played[to_my_right]
            visual_line[5][0] =  '               '
            visual_line[5][1] = f'  ╔ೱೱೱೱMMೱೱೱೱ╗  '
            visual_line[5][2] = f'  ║ {partner_card.face_value:<2}{partner_card.suit}     ║  '
            visual_line[5][3] = f'  ║ {partner_card.suit}       ║  '
            visual_line[5][4] = f'  ║   {partner_card.face_value:<2}{partner_card.suit}   ║  '
            visual_line[5][5] = f'  ║       {partner_card.suit} ║  '
            visual_line[5][6] = f'  ║     {partner_card.suit}{partner_card.face_value:>2} ║  '
            visual_line[5][7] = f'  ╚ೱೱೱೱWWೱೱೱೱ╝  '
            visual_line[5][8] = f'{partner_card.unicode} {partner_card.face_value:<3}{suit_name[partner_card.suit_number]:<8}{partner_card.unicode} '
        else:
            for x in range(0,9):
                visual_line[5][x] = '               '

    visual_line[6] = dict()
    visual_line[6][0] = f'{player_avatar[to_my_right][0]:<20}'
    visual_line[6][1] = f'{player_avatar[to_my_right][1]:<20}'
    visual_line[6][2] = f'{player_avatar[to_my_right][2]:<20}'
    visual_line[6][3] = f'{player_avatar[to_my_right][3]:<20}'
    visual_line[6][4] = f'{player_avatar[to_my_right][4]:<20}'
    visual_line[6][5] = f'{player_avatar[to_my_right][5]:<20}'
    visual_line[6][6] = f'{player_avatar[to_my_right][6]:<20}'
    visual_line[6][7] = f'{player_avatar[to_my_right][7]:<20}'
    visual_line[6][8] = f'{player_name[to_my_right]:<20}'

    if bidding:
        for x in range(0, 9):
            print(Fore.YELLOW + visual_line[0][x] + '                        ' +
                  Fore.BLACK + visual_line[3][x] + '                           ' +
                  Fore.YELLOW + visual_line[6][x])
    else:
        for x in range(0, 9):
            print(Fore.YELLOW + visual_line[0][x] + '  '+
                  Fore.BLACK + visual_line[2][x] + '                                     ' +
                  Fore.BLACK + visual_line[5][x] + '  ' +
                  Fore.YELLOW + visual_line[6][x])

    for x in range(0,2):
        print()

if __name__ == '__main__':
    #New Round; New Deck
    print(Fore.RED + chr(0xE008) + " Euchre from Home " + chr(0xE008))

    player_name = dict()
    player_name[0] = "Bart Simpson"
    player_name[1] = "Robot 579012A"
    player_name[2] = "Homer Simpson"
    player_name[3] = "Owley the Owl"

    player_avatar = dict()
    player_avatar[0] = bart
    player_avatar[1] = robot
    player_avatar[2] = homer
    player_avatar[3] = owl

    player = dict()
    for x in player_name:
        player[x] = Player(x,player_name, player_avatar)

    team_name = dict()
    team_name[0] = "Simpsons"
    team_name[1] = "Robot.OWL"

    dealer_id = 0
    game = Game(player_name, team_name, dealer_id)

    #Start playing cards
    print("let's start playing cards")
    while (game.check_winner() == 0):
        deck0 = new_deck()
        deck0 = shuffle_deck(deck0)
        player_hand = dict()
        player_hand[0] = []
        player_hand[1] = []
        player_hand[2] = []
        player_hand[3] = []
        discard_pile = []
        trump = set_trump('none')
        deal_deck()

        player_cards = dict()
        player_cards[0] = Hand(player_hand[0], trump)
        player_cards[1] = Hand(player_hand[1], trump)
        player_cards[2] = Hand(player_hand[2], trump)
        player_cards[3] = Hand(player_hand[3], trump)

        # Bidding:
        bidding_info = bidding(player_hand)
        round = Round(dealer_id)
        player_hand = bidding_info['post_bid_cards']
        # Play round
        round_score = play_round(bidding_info)
        dealer_id = (dealer_id + 1) % 4
    print("Game over.")
